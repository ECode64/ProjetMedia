package dao;

import java.util.ArrayList;
import java.util.Date;
import transfertObjet.Album;

/**
 *
 * @author E
 */
public interface DAOAlbums {

    /*Cette fct retourne la liste complete des albums. */
    ArrayList<Album> selectAlbums();
    
    /*Cette fct retourne liste trié par le code du album. */
    ArrayList<Album> selectAlbumsParCode(String codeA);
    
    /*Cette fct retourne liste trié par le nom du chanteur. */
    ArrayList<Album> selectAlbumsParNomCh(String nom);
    
    /*Cette fct retourne liste trié par le titre du album. */
    ArrayList<Album> selectAlbumsParTitreA(String titreA);
    
    /*Cette fct retourne liste trié entre deux dates du album. */
    ArrayList<Album> selectAlbumsParDate(Date d1, Date d2);
    
    /*Cette fct ajoute un element dans la BD. */
    boolean insertAlbum(Album al);
    
    /*Cette fct supprime un element de la BD. */
    boolean deleteAlbum(String codeA);
    
    /*Cette fct modifie un element de la BD. */
    boolean updateAlbum(Album al);
    
    /*Cette fct verifie si le code entre existe déjà dans la BD. */
    int verficationExisteCodeA(String code);
}
