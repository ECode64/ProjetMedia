package dao;

import java.util.ArrayList;
import transfertObjet.Chanteur;

/**
 *
 * @author E
 */
public interface DAOChanteurs {

    /*Cette fct retourne liste complete des chanteurs. */
    ArrayList<Chanteur> selectChanteurs();

    /*Cette fct ajoute un element dans la BD. */
    boolean insertChanteur(Chanteur ch);

    /*Cette fct supprime un element de la BD. */
    boolean deleteChanteur(int ch);

    /*Cette fct modifie un element de la BD. */
    boolean updateChanteur(Chanteur ch);

}
