package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import transfertObjet.Chanteur;

/**
 *
 * @author E
 */
public class TableModelChanteurs extends AbstractTableModel {

    /*Constructeur.*/
    public TableModelChanteurs(ArrayList<Chanteur> myList) {
        this.myList = myList;
    }

    /*Cette fct retourne le nombre des lignes.*/
    @Override
    public int getRowCount() {
        return myList.size();
    }

    /*Cette fct retourne le nombre de colonnes.*/
    @Override
    public int getColumnCount() {
        return 5;
    }

    /*Cette fct retourne un Object.*/
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Chanteur myC = myList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return myC.getIdentC();
            case 1:
                return myC.getNomC();
            case 2:
                return myC.getPrenomC();
            case 3:
                return myC.getSexeC();
            case 4:
                return myC.getDateNaissCFBE();
            default:
                return null;
        }
    }

    /*Cette fct retourne un album de la liste. */
    public void setMyList(ArrayList myList) {
        this.myList = myList;
        this.fireTableDataChanged();
    }

    /*Cette fct mofidie la liste.*/
    public Chanteur getMyList(int index) {
        return myList.get(index);
    }

    /*Cette fct retourne le nom d'une colonne.*/
    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }

    /*Déclaration des variables.*/
    private ArrayList<Chanteur> myList;
    private final String[] columnName = {"IdentC", "Nom", "Prénom", "Sexe", "Date Naiss"};
}
