package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import transfertObjet.Album;

/**
 *
 * @author E
 */
public class TableModelAlbum extends AbstractTableModel {

    /*Constructeur.*/
    public TableModelAlbum(ArrayList<Album> mylist) {
        this.myList = mylist;
    }

    /*Cette fct retourne le nombre des lignes.*/
    @Override
    public int getRowCount() {
        return myList.size();
    }

    /*Cette fct retourne le nombre de colonnes.*/
    @Override
    public int getColumnCount() {
        return 4;
    }

    /*Cette fct retourne un Object.*/
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Album myA = myList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return myA.getCodeA();
            case 1:
                return myA.getTitreA();
            case 2:
                return myA.getDateSortieAFBE();
            case 3:
                return myA.getChanteurA();
            default:
                return null;
        }
    }

    /*Cette fct retourne un album de la liste. */
    public Album getMyList(int index) {
        return myList.get(index);
    }

    /*Cette fct mofidie la liste.*/
    public void setMyList(ArrayList myList) {
        this.myList = myList;
        this.fireTableDataChanged();
    }

    /*Cette fct retourne le nom d'une colonne.*/
    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }

    /*Déclaration des variables.*/
    private ArrayList<Album> myList;
    private final String[] columnName = {"Code", "Titre", "Date sortie", "Nom Chanteur"};

}
