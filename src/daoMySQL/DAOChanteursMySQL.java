/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoMySQL;

import dao.DAOChanteurs;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import transfertObjet.Chanteur;

/**
 *
 * @author E
 */
public class DAOChanteursMySQL implements DAOChanteurs {

    private static DAOChanteursMySQL uniqueInstance = new DAOChanteursMySQL();

    public static DAOChanteursMySQL getInstance() {
        return uniqueInstance;
    }

    /*Liste des chanteurs.*/
    @Override
    public ArrayList<Chanteur> selectChanteurs() {
        ArrayList<Chanteur> myList = new ArrayList<>();
        String req = "SELECT identC,UPPER (nomC), UPPER(prenomC), sexeC, naissC FROM chanteur"
                + " order by 1";
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Chanteur(res.getInt(1), res.getString(2), res.getString(3), res.getString(4).charAt(0), res.getDate(5)));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Ajout d'un element.*/
    @Override
    public boolean insertChanteur(Chanteur ch) {
        return ConnexionMySQL.getInstance().actionQuery("INSERT INTO chanteur (identC, nomC, prenomC, sexeC, naissC)"
                + " VALUES (0,'" + ch.getNomC().toUpperCase() + "','" + ch.getPrenomC().toUpperCase()
                + "','" + ch.getSexeC() + "'," + getDateSQL(ch.getNaissC()) + ")");
    }

    /*Suppretion d'un element. */
    @Override
    public boolean deleteChanteur(int ch) {
        return ConnexionMySQL.getInstance().actionQuery("DELETE FROM chanteur WHERE identC = '" + ch + "'");
    }

    /*Modification d'un element.*/
    @Override
    public boolean updateChanteur(Chanteur ch) {
        return ConnexionMySQL.getInstance().actionQuery("Update chanteur set identC = '" + ch.getIdentC()
                + "', nomC = '" + ch.getNomC().toUpperCase() + "', prenomC = '" + ch.getPrenomC().toUpperCase()
                + "', sexeC = '" + ch.getSexeC() + "', naissC = " + getDateSQL(ch.getNaissC()) + " where identC = '"
                + ch.getIdentC() + "'");
    }

    /*Conversion Date en DateSQL*/
    private String getDateSQL(Date d) {
        String tmp;
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        tmp = "'" + dateParser.format(d) + "'";
        return tmp;
    }

}
