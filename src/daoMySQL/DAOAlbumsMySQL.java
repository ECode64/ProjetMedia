package daoMySQL;

import dao.DAOAlbums;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import transfertObjet.Album;
import transfertObjet.Chanteur;

/**
 *
 * @author E
 */
public class DAOAlbumsMySQL implements DAOAlbums {

    private static DAOAlbumsMySQL uniqueInstance = new DAOAlbumsMySQL();

    public static DAOAlbumsMySQL getInstance() {
        return uniqueInstance;
    }

    /*Liste des albums.*/
    @Override
    public ArrayList<Album> selectAlbums() {
        ArrayList<Album> myList = new ArrayList<>();
        String req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                + "where A.identC = C.identC "
                + " order by 1";
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Album(res.getString(1), res.getString(2), res.getDate(3),
                        new Chanteur(res.getInt(4), res.getString(5), res.getString(6), res.getString(7).charAt(0), res.getDate(8))));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Liste trie par code.*/
    @Override
    public ArrayList<Album> selectAlbumsParCode(String codeA) {

        ArrayList<Album> myList = new ArrayList<>();
        String req;
        if (codeA.equals("*")) {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC"
                    + " order by 1";
        } else {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC and  codeA = '" + codeA.toUpperCase() + "'"
                    + " order by 1";
        }
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Album(res.getString(1), res.getString(2), res.getDate(3),
                        new Chanteur(res.getInt(4), res.getString(5), res.getString(6), res.getString(7).charAt(0), res.getDate(8))));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Liste trie par nom.*/
    @Override
    public ArrayList<Album> selectAlbumsParNomCh(String nom) {

        ArrayList<Album> myList = new ArrayList<>();
        String req;
        if (nom.equals("*")) {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, UPPER(C.identC), "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC"
                    + " order by 1";
        } else {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, UPPER(C.identC), "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC and C.nomC = '" + nom.toUpperCase() + "'"
                    + " order by 1";
        }
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Album(res.getString(1), res.getString(2), res.getDate(3),
                        new Chanteur(res.getInt(4), res.getString(5), res.getString(6), res.getString(7).charAt(0), res.getDate(8))));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Liste trie par titre. */
    @Override
    public ArrayList<Album> selectAlbumsParTitreA(String titreA) {

        ArrayList<Album> myList = new ArrayList<>();
        String req;
        if (titreA.equals("*")) {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC"
                    + " order by 1";
        } else {
            req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                    + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                    + "where A.identC = C.identC and A.titreA LIKE '%" + titreA.toUpperCase() + "%'"
                    + " order by 1";
        }
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Album(res.getString(1), res.getString(2), res.getDate(3),
                        new Chanteur(res.getInt(4), res.getString(5), res.getString(6), res.getString(7).charAt(0), res.getDate(8))));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Liste trie entre deux dates*/
    @Override
    public ArrayList<Album> selectAlbumsParDate(Date d1, Date d2) {
        ArrayList<Album> myList = new ArrayList<>();
        String req;
        req = "SELECT UPPER(codeA), UPPER(titreA), sortieA, C.identC, "
                + "C.nomC, C.prenomC ,C.sexeC,C.naissC FROM album A, chanteur C "
                + "WHERE C.identC = A.identC AND sortieA BETWEEN " + getDateSQL(d1) + " AND " + getDateSQL(d2)
                + " group by sortieA"
                + " order by 1";
        ResultSet res = ConnexionMySQL.getInstance().selectQuery(req);
        try {
            while (res.next()) {
                myList.add(new Album(res.getString(1), res.getString(2), res.getDate(3),
                        new Chanteur(res.getInt(4), res.getString(5), res.getString(6), res.getString(7).charAt(0), res.getDate(8))));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return myList;
    }

    /*Ajout d'un element.*/
    @Override
    public boolean insertAlbum(Album al) {
        return ConnexionMySQL.getInstance().actionQuery("INSERT INTO album (codeA, titreA, sortieA, identC)"
                + " VALUES ('" + al.getCodeA().toUpperCase() + "','" + al.getTitreA().toUpperCase()
                + "'," + getDateSQL(al.getSortieA()) + ",'" + al.getChanteurA().getIdentC() + "')");
    }

    /*Suppretion d'un element. */
    @Override
    public boolean deleteAlbum(String codeA) {
        return ConnexionMySQL.getInstance().actionQuery("DELETE FROM album WHERE codeA = '" + codeA + "'");
    }

    /*Modification d'un element.*/
    @Override
    public boolean updateAlbum(Album al) {
        return ConnexionMySQL.getInstance().actionQuery("Update album set codeA = '" + al.getCodeA().toUpperCase()
                + "', titreA = '" + al.getTitreA().toUpperCase() + "', sortieA = " + getDateSQL(al.getSortieA())
                + ", identC = '" + al.getChanteurA().getIdentC() + "'"
                + " where codeA = '" + al.getCodeA().toUpperCase() + "'");
    }
    
    /*Recheche code exitant*/
    @Override
    public int verficationExisteCodeA(String code) {
        ResultSet res = ConnexionMySQL.getInstance().selectQuery("SELECT COUNT(codeA) FROM album WHERE codeA = '" + code + "'");
        int r = -1;
        try {
            while (res.next()) {
                r = res.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAlbumsMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return r;
    }

    /*Conversion Date en DateSQL*/
    private String getDateSQL(Date d) {
        String tmp;
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        tmp = "'" + dateParser.format(d) + "'";
        return tmp;
    }
}
