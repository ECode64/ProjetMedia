/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import dao.Connexion;
import dao.DAOAlbums;
import dao.DAOChanteurs;
import daoMySQL.ConnexionMySQL;
import daoMySQL.DAOAlbumsMySQL;
import daoMySQL.DAOChanteursMySQL;

/**
 *
 * @author E
 */
public class Factory {

    public static DAOChanteurs getDAOChanteurs() {
        return DAOChanteursMySQL.getInstance();
    }

    public static DAOAlbums getDAOAlbums() {
        return DAOAlbumsMySQL.getInstance();
    }

    public static Connexion getDAOConnexion() {
        return ConnexionMySQL.getInstance();
    }
}
