/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transfertObjet;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author E
 */
public class Album {

    /**
     *
     */
    public Album() {
    }

    /**
     *Constructor
     * @param codeA
     * @param titreA
     * @param sortieA
     * @param identC
     */
    public Album(String codeA, String titreA, Date sortieA, Chanteur identC) {
        this.codeA = codeA;
        this.titreA = titreA;
        this.sortieA = sortieA;
        this.chanteurA = identC;
    }

    /**
     * Retourne le code de l'album. 
     * @return String
     */
    public String getCodeA() {
        return codeA;
    }

    /**
     * Modifie le code de l'album.
     * @param codeA
     */
    public void setCodeA(String codeA) {
        this.codeA = codeA;
    }

    /**
     * Retourne le titre de l'album.
     * @return String
     */
    public String getTitreA() {
        return titreA;
    }

    /**
     * Modifie le titre de l'album.
     * @param titreA
     */
    public void setTitreA(String titreA) {
        this.titreA = titreA;
    }

    /**
     * Retourne la date de sortie de l'album.
     * @return Date.
     */
    public Date getSortieA() {
        return sortieA;
    }

    /**
     * Modifie la date de sortie de l'album par celle qui est passé en param.
     * @param sortieA
     */
    public void setSortieA(Date sortieA) {
        this.sortieA = sortieA;
    }

    /**
     * 
     * @return
     */
    public Chanteur getChanteurA() {
        return chanteurA;
    }

    /**
     *
     * @param chanteurA
     */
    public void setChanteurA(Chanteur chanteurA) {
        this.chanteurA = chanteurA;
    }

    /**
     *
     * @return
     */
    public String getDateSortieAFBE() {
        String tmp;
        if (this.sortieA == null) {
            tmp = "";
        } else {
            SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
            tmp = sm.format(sortieA);
        }
        return tmp;
    }

    @Override
    public String toString() {
        return codeA;
    }

    private String codeA;
    private String titreA;
    private Date sortieA;
    private Chanteur chanteurA;
}
