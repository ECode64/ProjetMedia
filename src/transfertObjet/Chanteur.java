/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transfertObjet;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author E
 */
public class Chanteur {

    public Chanteur() {
    }

    public Chanteur(int identC, String nomC, String prenomC, char sexeC, Date naissC) {
        this.identC = identC;
        this.nomC = nomC;
        this.prenomC = prenomC;
        this.sexeC = sexeC;
        this.naissC = naissC;
    }

    public int getIdentC() {
        return identC;
    }

    public void setIdentC(int identC) {
        this.identC = identC;
    }

    public String getNomC() {
        return nomC;
    }

    public void setNomC(String nomC) {
        this.nomC = nomC;
    }

    public String getPrenomC() {
        return prenomC;
    }

    public void setPrenomC(String prenomC) {
        this.prenomC = prenomC;
    }

    public char getSexeC() {
        return sexeC;
    }

    public void setSexeC(char sexeC) {
        this.sexeC = sexeC;
    }

    public Date getNaissC() {
        return naissC;
    }

    public void setNaissC(Date naissC) {
        this.naissC = naissC;
    }

    public String getDateNaissCFBE() {
        String tmp;
        if (this.naissC == null) {
            tmp = "";
        } else {
            SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
            tmp = sm.format(naissC);
        }
        return tmp;
    }

    @Override
    public String toString() {
        return nomC.toUpperCase();
    }

    private int identC;
    private String nomC;
    private String prenomC;
    private char sexeC;
    private Date naissC;
}
