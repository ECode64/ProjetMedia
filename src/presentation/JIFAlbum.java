package presentation;

import dao.DAOAlbums;
import dao.DAOChanteurs;
import factory.Factory;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import model.TableModelAlbum;
import transfertObjet.Album;
import transfertObjet.Chanteur;

/**
 *
 * @author E
 */
public class JIFAlbum extends javax.swing.JInternalFrame {

    /**
     * Creates new form JIFAlbum
     */
    public JIFAlbum() {
        initComponents();
        fillComponets();
    }

    private void fillComponets() {

        ArrayList<Chanteur> listeCh = daoCh.selectChanteurs();
        ArrayList<Album> listeAls = daoAl.selectAlbums();
        listeCh.add(0, new Chanteur(0, "*", null, ' ', null));
        listeAls.add(0, new Album("*", null, null, null));

        listeAls.forEach((cod) -> {
            jCBCodeA.addItem(cod);
        });

        listeCh.forEach((ch) -> {
            jCBNomC.addItem(ch);
        });

        getDateMin();
        getDateMax();

    }

    private void triModelParCodeAlbum() {
        Album selectedAlb = (Album) jCBCodeA.getSelectedItem();

        if (selectedAlb != null) {
            myModel.setMyList(daoAl.selectAlbumsParCode(selectedAlb.getCodeA()));
        }
    }

    private void triModelParNomCh() {
        Chanteur selectedCh = (Chanteur) jCBNomC.getSelectedItem();

        if (selectedCh != null) {
            myModel.setMyList(daoAl.selectAlbumsParNomCh(selectedCh.getNomC()));
        }
    }

    private void triModelParTitre() {
        myModel.setMyList(daoAl.selectAlbumsParTitreA(jTFTitreA.getText()));

    }

    private void triModelParDate() {
        myModel.setMyList(daoAl.selectAlbumsParDate(jDCDateMin.getDate(), jDCDateMax.getDate()));
    }

    private void refreshModel() {
        myModel.setMyList(daoAl.selectAlbums());
    }

    private void getDateMin() {
        Calendar c = Calendar.getInstance();

        c.set(1950, 00, 01);
        jDCDateMin.getDateEditor().setDate(c.getTime());
    }

    private void getDateMax() {
        jDCDateMax.getDateEditor().setDate(new Date());
    }

    private void supprimerElement() {
        if (jTable1.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Sélectionnez d'abord une ligne svp !", "Avertissement", JOptionPane.ERROR_MESSAGE);
        } else {
            int rep = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment supprimer "
                    + myModel.getValueAt(jTable1.getSelectedRow(), 0) + " ?", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (rep == JOptionPane.YES_OPTION) {
                daoAl.deleteAlbum((String) myModel.getValueAt(jTable1.getSelectedRow(), 0));
            } else {
                refreshModel();
            }
        }
        refreshModel();
    }

    private void modifierElement() {
        if (jTable1.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Sélectionnez d'abord une ligne svp !", "Avertissement", JOptionPane.ERROR_MESSAGE);
        } else {
            // renvoie l'objet Appareil à passer en paramètre
            Album al = myModel.getMyList(jTable1.getSelectedRow());

            // récupère la JFrame parente
            JFrame parent = (JFrame) (this.getDesktopPane().getTopLevelAncestor());
            new JDFModifierAl(parent, al);

            // après la modification : rafraîchir les données en relisant la BD
            refreshModel();
        }
    }

    private void valParDefault() {
        jCBCodeA.setSelectedIndex(0);
        jCBNomC.setSelectedIndex(0);
        jTFTitreA.setText("");
        getDateMin();
        getDateMax();
    }

    private void ChapsVisible(boolean code, boolean nom, boolean titre, boolean date) {
        jCBCodeA.setEnabled(code);
        jCBNomC.setEnabled(nom);
        jTFTitreA.setEditable(titre);
        jDCDateMin.setEnabled(date);
        jDCDateMax.setEnabled(date);
        jBChercher.setEnabled(date);
    }

    private void filtrerParOptions() {
        switch (jCBFiltrer.getSelectedIndex()) {
            case 0:
                ChapsVisible(false, false, false, false);
                valParDefault();
                break;
            case 1:
                ChapsVisible(true, false, false, false);
                valParDefault();
                break;
            case 2:
                ChapsVisible(false, true, false, false);
                valParDefault();
                break;
            case 3:
                ChapsVisible(false, false, true, false);
                valParDefault();
                break;
            case 4:
                ChapsVisible(false, false, false, true);
                valParDefault();
                break;
        }
        refreshModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new JTable(myModel);
        jPanel2 = new javax.swing.JPanel();
        jLTitreA = new javax.swing.JLabel();
        jTFTitreA = new javax.swing.JTextField();
        jCBNomC = new javax.swing.JComboBox();
        jCBCodeA = new javax.swing.JComboBox();
        jLChercher = new javax.swing.JLabel();
        jLEt = new javax.swing.JLabel();
        jDCDateMin = new com.toedter.calendar.JDateChooser();
        jDCDateMax = new com.toedter.calendar.JDateChooser();
        jBChercher = new javax.swing.JButton();
        jCBFiltrer = new javax.swing.JComboBox();
        jLNomCh = new javax.swing.JLabel();
        jLCodeA = new javax.swing.JLabel();
        jLFiltrer = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jBSupprimer = new javax.swing.JButton();
        jBModifier = new javax.swing.JButton();
        jBAjouter = new javax.swing.JButton();

        setBorder(null);
        setMinimumSize(new java.awt.Dimension(664, 517));
        setVisible(true);

        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jLTitreA.setText("Titre:");

        jTFTitreA.setEditable(false);
        jTFTitreA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFTitreAActionPerformed(evt);
            }
        });

        jCBNomC.setEnabled(false);
        jCBNomC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBNomCActionPerformed(evt);
            }
        });

        jCBCodeA.setEnabled(false);
        jCBCodeA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBCodeAActionPerformed(evt);
            }
        });

        jLChercher.setText("Chercher entre :");

        jLEt.setText("et");

        jDCDateMin.setEnabled(false);

        jDCDateMax.setEnabled(false);

        jBChercher.setText("Chercher");
        jBChercher.setEnabled(false);
        jBChercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBChercherActionPerformed(evt);
            }
        });

        jCBFiltrer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Code", "Nom", "Titre", "Date" }));
        jCBFiltrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBFiltrerActionPerformed(evt);
            }
        });

        jLNomCh.setText("Nom :");

        jLCodeA.setText("Code:");

        jLFiltrer.setText("Filtrer par:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLFiltrer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCBFiltrer, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLCodeA)
                .addGap(18, 18, 18)
                .addComponent(jCBCodeA, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLNomCh)
                .addGap(18, 18, 18)
                .addComponent(jCBNomC, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLTitreA)
                .addGap(18, 18, 18)
                .addComponent(jTFTitreA, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLChercher)
                .addGap(18, 18, 18)
                .addComponent(jDCDateMin, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLEt)
                .addGap(18, 18, 18)
                .addComponent(jDCDateMax, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBChercher)
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jCBCodeA, jCBNomC});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBChercher)
                    .addComponent(jDCDateMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCBCodeA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLChercher)
                        .addComponent(jCBNomC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTFTitreA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLTitreA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jCBFiltrer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLNomCh)
                        .addComponent(jLCodeA)
                        .addComponent(jLFiltrer))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLEt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jDCDateMin, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jBSupprimer.setText("Supprimer");
        jBSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSupprimerActionPerformed(evt);
            }
        });

        jBModifier.setText("Modifier");
        jBModifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBModifierActionPerformed(evt);
            }
        });

        jBAjouter.setText("Ajouter");
        jBAjouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAjouterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(878, Short.MAX_VALUE)
                .addComponent(jBAjouter)
                .addGap(18, 18, 18)
                .addComponent(jBModifier, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBSupprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jBAjouter, jBModifier, jBSupprimer});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBSupprimer)
                    .addComponent(jBModifier, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBAjouter))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jBAjouter, jBModifier, jBSupprimer});

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCBCodeAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBCodeAActionPerformed
        triModelParCodeAlbum();
    }//GEN-LAST:event_jCBCodeAActionPerformed

    private void jCBNomCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBNomCActionPerformed
        triModelParNomCh();
    }//GEN-LAST:event_jCBNomCActionPerformed

    private void jTFTitreAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFTitreAActionPerformed
        triModelParTitre();
    }//GEN-LAST:event_jTFTitreAActionPerformed

    private void jBChercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBChercherActionPerformed
        triModelParDate();
    }//GEN-LAST:event_jBChercherActionPerformed

    private void jBSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSupprimerActionPerformed
        supprimerElement();
    }//GEN-LAST:event_jBSupprimerActionPerformed

    private void jBAjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAjouterActionPerformed
        Frame JIFAlbum = new Frame();
        new JDFAjouterAl(JIFAlbum, true);
        refreshModel();
    }//GEN-LAST:event_jBAjouterActionPerformed

    private void jBModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBModifierActionPerformed
        modifierElement();
    }//GEN-LAST:event_jBModifierActionPerformed

    private void jCBFiltrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBFiltrerActionPerformed
        filtrerParOptions();
    }//GEN-LAST:event_jCBFiltrerActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAjouter;
    private javax.swing.JButton jBChercher;
    private javax.swing.JButton jBModifier;
    private javax.swing.JButton jBSupprimer;
    private javax.swing.JComboBox jCBCodeA;
    private javax.swing.JComboBox jCBFiltrer;
    private javax.swing.JComboBox jCBNomC;
    private com.toedter.calendar.JDateChooser jDCDateMax;
    private com.toedter.calendar.JDateChooser jDCDateMin;
    private javax.swing.JLabel jLChercher;
    private javax.swing.JLabel jLCodeA;
    private javax.swing.JLabel jLEt;
    private javax.swing.JLabel jLFiltrer;
    private javax.swing.JLabel jLNomCh;
    private javax.swing.JLabel jLTitreA;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTFTitreA;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
    private final DAOAlbums daoAl = Factory.getDAOAlbums();
    private final DAOChanteurs daoCh = Factory.getDAOChanteurs();
    private final TableModelAlbum myModel = new TableModelAlbum(daoAl.selectAlbums());

}
