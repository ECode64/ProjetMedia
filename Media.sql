CREATE DATABASE `media` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `media`;

--
-- Creation de la table `chanteur`
--

CREATE TABLE IF NOT EXISTS `chanteur` (
	`identC` INT(100) NOT NULL AUTO_INCREMENT,
	`nomC` VARCHAR(30) COLLATE utf8_unicode_ci DEFAULT NULL,
	`prenomC` VARCHAR(30) COLLATE utf8_unicode_ci DEFAULT NULL,
	`sexeC` CHAR(1) DEFAULT NULL,
	`naissC` DATETIME DEFAULT NULL,
	PRIMARY KEY (`IdentC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `chanteur`
--

INSERT INTO `chanteur` (`identC`, `nomC`, `prenomC`, `sexeC`, `naissC`) VALUES
	(NULL, 'Hallyday','Johnny', 'M', '1943-06-15 00:00:00'),
	(NULL, 'Jackson', 'Michael ', 'M', '1958-10-29 00:00:00'),
	(NULL, 'Dion', 'Céline', 'F', '1968-03-30 00:00:00'),
	(NULL, 'Presley', 'Elvis', 'M', '1935-01-08 00:00:00'),
	(NULL, 'Lopez', 'Jennifer', 'F', '1969-07-24 00:00:00');

--
-- Creation de la table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
	`codeA` VARCHAR(5) COLLATE utf8_unicode_ci NOT NULL,
	`titreA` VARCHAR (30) COLLATE utf8_unicode_ci DEFAULT NULL,
	`sortieA` DATETIME DEFAULT NULL,
	`identC` INT (100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Contenu de la table `album`
--

INSERT INTO `album` (`codeA`, `titreA`, `sortieA`, `identC`) VALUES
	('P0001', 'On the 6', '1999-05-28 00:00:00', 5),
	('P0002', 'J.Lo', '2001-01-23 00:00:00', 5),
	('P0003', 'Rester Vivant Tour', '2016-10-21 00:00:00',1),
	('P0004', 'Thriller','1982-11-30 00:00:00',2),
	('P0005', 'Forever, Michael', '1975-01-16 00:00:00',2);
	
ALTER TABLE `chanteur`
  ADD CONSTRAINT `CHK_SexeC` CHECK (sexeC ='M' OR sexeC ='F');
 
ALTER TABLE `album`
ADD CONSTRAINT `album_1` FOREIGN KEY (`identC`) REFERENCES `chanteur` (`identC`);